# EMPDV (Enhanced Microprocessor Design with VLIW) Specification
Version 0.0.1

# Summary
* 64 Bit VLIW Architecture Design
* 60 General Purpose Registers (GPR0 to GPR59)
* 64 Floating-Point Registers (FPR0 to FPR63)
* Program Counter, Instruction Register and Stack Pointer
* Memory Buffer and Memory Address registers
* Big Endian

Registers:
* GPR0 to GPR59 - General Purpose Registers (GPR)
* IR (GPR60) - Instruction Register
* SP (GPR61) - Stack Pointer
* MB (GPR62) - Memory Buffer
* MD (GPR63) - Memory Address
* PC - Program Counter
